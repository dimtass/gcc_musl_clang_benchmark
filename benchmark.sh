#!/bin/bash

# clean previous builds
rm -rf output/

: ${KEEP:=false}

CFILE="$*"
FLAGS="-w"

echo "Testing file: ${CFILE}"
mkdir output

# Test GCC
gcc ${CFILE} ${FLAGS} -o output/output_gcc > /dev/null
echo "gcc            : " `wc -c < output/output_gcc`

gcc -Os ${CFILE} ${FLAGS} -o output/output_gcc_Os > /dev/null
echo "gcc -Os        : " `wc -c < output/output_gcc_Os`

gcc -O3 ${CFILE} ${FLAGS} -o output/output_gcc_O3 > /dev/null
echo "gcc -O3        : " `wc -c < output/output_gcc_O3`

gcc -flto ${CFILE} ${FLAGS} -o output/output_gcc_flto > /dev/null
echo "gcc -flto      : " `wc -c < output/output_gcc_flto`

gcc -flto -Os ${CFILE} ${FLAGS} -o output/output_gcc_flto_Os > /dev/null
echo "gcc -flto -Os  : " `wc -c < output/output_gcc_flto_Os`

gcc -flto -O3 ${CFILE} ${FLAGS} -o output/output_gcc_flto_O3 > /dev/null
echo "gcc -flto -O3  : " `wc -c < output/output_gcc_flto_O3`

# Test musl
/usr/local/musl/bin/musl-gcc ${CFILE} ${FLAGS} -o output/output_musl > /dev/null
echo "musl           : " `wc -c < output/output_musl`

/usr/local/musl/bin/musl-gcc -Os ${CFILE} ${FLAGS} -o output/output_musl_Os > /dev/null
echo "musl -Os       : " `wc -c < output/output_musl_Os`

/usr/local/musl/bin/musl-gcc -O3 ${CFILE} ${FLAGS} -o output/output_musl_O3 > /dev/null
echo "musl -O3       : " `wc -c < output/output_musl_O3`

/usr/local/musl/bin/musl-gcc -flto ${CFILE} ${FLAGS} -o output/output_musl_flto > /dev/null
echo "musl -flto     : " `wc -c < output/output_musl_flto`

/usr/local/musl/bin/musl-gcc -flto -Os ${CFILE} ${FLAGS} -o output/output_musl_flto_Os > /dev/null
echo "musl -flto -Os : " `wc -c < output/output_musl_flto_Os`

/usr/local/musl/bin/musl-gcc -flto -O3 ${CFILE} ${FLAGS} -o output/output_musl_flto_O3 > /dev/null
echo "musl -flto -O3 : " `wc -c < output/output_musl_flto_O3`

# Test clang
clang -w ${CFILE} ${FLAGS} -o output/output_clang > /dev/null
echo "clang          : " `wc -c < output/output_clang`

clang -w -Os ${CFILE} ${FLAGS} -o output/output_clang_Os > /dev/null
echo "clang -Os      : " `wc -c < output/output_clang_Os`

clang -w -O3 ${CFILE} ${FLAGS} -o output/output_clang_O3 > /dev/null
echo "clang -O3      : " `wc -c < output/output_clang_O3`

# Test clang+musl
./musl-clang.sh -w ${CFILE} ${FLAGS} -o output/output_clang_musl > /dev/null
echo "clang+musl     : " `wc -c < output/output_clang_musl`

./musl-clang.sh -w -Os ${CFILE} ${FLAGS} -o output/output_clang_musl_Os > /dev/null
echo "clang+musl -Os : " `wc -c < output/output_clang_musl_Os`

./musl-clang.sh -w -O3 ${CFILE} ${FLAGS} -o output/output_clang_musl_O3 > /dev/null
echo "clang+musl -O3 : " `wc -c < output/output_clang_musl_O3`

if [ ${KEEP} == false ] ; then
    rm -rf test0*
fi
