#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define TRACE(X) printf X

void set_value(char * p, char value)
{
    *p = value;
}

int main(void)
{
    srand(time(NULL));
    int p_size = rand()%500;
    char * p = (char*) malloc(p_size);
    
    if (!p) {
        TRACE(("malloc failed.\n"));
        goto _exit;
    }

    for (int i=0; i<p_size; i++) {
        set_value(&p[i], rand()%0xff);
    }

    for (int i=0; i<100; i++) {
        TRACE(("%02X,", 0xff & p[i]));
    }
    TRACE(("\n"));

_exit:
    if (p) {
        free(p);
    }

    return 0;
}
